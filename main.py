import streamlit as st
import time
import pandas as pd
import jieba
# from collections import Counter
def cut_word(text):
    return list(jieba.cut(str(text), cut_all=False))

st.set_page_config(
    page_title="热词分析",
    page_icon=":robot:",
    layout="wide",
)

st.markdown('# 热词分析')
st.markdown('## 1. 下载热词分析文件模板')
with open('热词分析模板.xlsx','rb') as f:
   st.download_button('下载热词分析模板', f,file_name='热词分析模板.xlsx')

st.markdown('## 2. 上传热词分析文件')
uploaded_file = st.file_uploader("选择文件",type='xlsx')

if uploaded_file is not None:
    upload_frame = pd.read_excel(uploaded_file)
    if st.button('开始分析'):
        words_tmp = upload_frame['分词内容'].apply(cut_word)
        words = [word for words in words_tmp for word in words]
        # result=Counter(words)
        # result_df = pd.DataFrame(result.most_common(20),columns=['词','频次'])
        result=pd.value_counts(words)
        st.markdown('## 3. 分词结果')
        st.dataframe(result,use_container_width=True)