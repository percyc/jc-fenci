# 使用说明
## 1. 下载代码
```bash
git clone https://gitee.com/percyc/jc-fenci.git
```
## 2. 创建virtualenv
```bash
cd jc-fenci
virtualenv venv
```
## 3. 安装依赖
```bash
source venv/bin/activate
pip install -r requirements.txt
```
## 4. 运行
```bash
streamlit run main.py
```

# Docker部署
## 1. 构建镜像
```bash
docker build -t jc-fenci .
```
## 2. 运行
```bash
docker run -p 8501:8501 -d jc-fenci
```